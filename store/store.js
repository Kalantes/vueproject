import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        users: [
            {

                name: "User_1",
                password: "test",
                detail: "Info 1"
            },
        ]
    },

    getters: {
        userList: state => {
            return state.users;
        },

        user: (state) => (name) => {
            return state.users.find((user) => user.name == name);
        }
    },

    mutations: {
        userDetail(state, payload) {
            payload.user.detail = payload.value;
        },

        addUser(state, payload) {
            state.users.push(payload);
        }
    },

    actions: {
        setUserDetail(context, payload) {
            var user = context.getters.user(payload.id);

            context.commit('userDetail', {user: user, value: payload.value});
        },

        addUser(context, payload) {
            context.commit('addUser', payload);
        }
    }
});
