import Music from './components/Music.vue';
import Home from './components/Home.vue';
import Register from './components/Register';
import User from './components/User.vue';



export const routes = [
    {path: '', component: Home},
    {path: '/music', component: Music},
    {path: '/user', component: User, children: [
            {path: '/register', component: Register}
        ]},
]