import Vue from 'vue'
import Vuex from 'vuex';
export const EventBus = new Vue();

import App from './App.vue'
import VRouter from 'vue-router'
import { routes } from './routes.js'

Vue.use(VRouter);
Vue.config.productionTip = false


const router = new VRouter({
    routes,
    mode:'history'
});

new Vue({
    router,
  render: h => h(App),
}).$mount('#app')
